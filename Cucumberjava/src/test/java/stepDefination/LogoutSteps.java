package stepDefination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.*;

public class LogoutSteps {
	WebDriver driver = null;

	@SuppressWarnings("deprecation")
	WebDriverWait wait = new WebDriverWait(driver, 10);
	
	@Given("user is on the home page")
	public void user_is_on_the_home_page() {String projectPath = System.getProperty("user.dir");
	System.getProperty("webdriver.chrome.driver",projectPath+"/src/test/resources/Drivers/chromedriver.exe");
	driver = new ChromeDriver();	
	driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
	
	driver.navigate().to("http://demo.guru99.com/V4/");
	driver.findElement(By.name("uid")).sendKeys("mngr275213");
    driver.findElement(By.name("password")).sendKeys("vEsyzyd");
    driver.findElement(By.name("btnLogin")).click();
    driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
}

	@When("user clicks on logout tap")
	public void user_clicks_on_logout_tap() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		driver.findElement(By.xpath("/html/body/div[3]/div/ul/li[15]/a")).click();
	}

	@Then("user is navigated to login page")
	public void user_is_navigated_to_login_page() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("uid))")));
		driver.getPageSource().contains("Guru99 Bank");
	}

}
