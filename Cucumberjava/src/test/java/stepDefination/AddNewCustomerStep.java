package stepDefination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;

public class AddNewCustomerStep {
	WebDriver driver = null;
	
	@Given("user is on home page")
	public void user_is_on_home_page() {
		String projectPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver",projectPath+"/src/test/resources/Drivers/chromedriver.exe");
		driver = new ChromeDriver();	
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		
		driver.navigate().to("http://demo.guru99.com/V4/");
		driver.findElement(By.name("uid")).sendKeys("mngr275213");
	    driver.findElement(By.name("password")).sendKeys("vEsyzyd");
	    driver.findElement(By.name("btnLogin")).click();
	    driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
	}

	@When("user clicks on New Customer tap")
	public void user_clicks_on_New_Customer_tap() {
		driver.findElement(By.xpath("/html/body/div[3]/div/ul/li[2]/a")).click();
		driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
	}

	@And("user enters number in customer name")
	public void user_enters_number_in_customer_name() {
		 driver.findElement(By.name("name")).sendKeys("123456");
	}

	@Then("Numbers are not allowed message is displayed")
	public void Numbers_are_not_allowed_message_is_displayed() {
	   driver.getPageSource().contains("Numbers are not allowed");
	   
	   driver.close();
	   driver.quit();
	}



}
