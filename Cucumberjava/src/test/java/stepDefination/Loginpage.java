package stepDefination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;

public class Loginpage {

	WebDriver driver = null;
	
	@Given("user is on login page")
	public void user_is_on_login_page() {
		String projectPath = System.getProperty("user.dir");
		System.getProperty("webdriver.chrome.driver",projectPath+"/src/test/resources/Drivers/chromedriver.exe");
		driver = new ChromeDriver();	
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		
		driver.navigate().to("http://demo.guru99.com/V4/");
	}

	@When("user enters username and password")
	public void user_enters_username_and_password() {
	    driver.findElement(By.name("uid")).sendKeys("mngr275213");
	    driver.findElement(By.name("password")).sendKeys("vEsyzyd");
	    

	}

	@And("clicks on login button")
	public void clicks_on_login_button() {
		 driver.findElement(By.name("btnLogin")).click();
	}

	@Then("user is navigated to the home page")
	public void user_is_navigated_to_the_home_page() {
		driver.getPageSource().contains("Manger Id");
		driver.close();
		driver.quit();
	}

}
